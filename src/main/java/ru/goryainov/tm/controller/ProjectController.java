package ru.goryainov.tm.controller;

import ru.goryainov.tm.repository.ProjectRepository;
import ru.goryainov.tm.entity.Project;
import ru.goryainov.tm.service.ProjectService;

public class ProjectController extends  AbstractController{

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Вывод создания проекта
     */
    public int createProject() {
        System.out.println("[Create project]");
        System.out.println("[Please, enter project name:]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanner.nextLine();
        projectService.create(name, description);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение проекта по индексу
     */
    public int updateProjectByIndex() {
        System.out.println("[Update project]");
        System.out.println("[Please, enter project index:]");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter project name:]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Изменение проекта по идентификатору
     */
    public int updateProjectById() {
        System.out.println("[Update project]");
        System.out.println("[Please, enter project id:]");
        final long id = Long.parseLong(scanner.nextLine());
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[Fail]");
            return 0;
        }
        System.out.println("[Please, enter project name:]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter project description:]");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по имени
     */
    public int removeProjectByName() {
        System.out.println("[Remove project by name]");
        System.out.println("[Please, enter project name:]");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по идентификатору
     */
    public int removeProjectById() {
        System.out.println("[Remove project by id]");
        System.out.println("[Please, enter project id:]");
        final long id = scanner.nextLong();
        final Project project = projectService.removeById(id);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Удаление проекта из списка по индексу
     */
    public int removeProjectByIndex() {
        System.out.println("[Remove project by index]");
        System.out.println("[Please, enter project index:]");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) System.out.println("[Fail]");
        else System.out.println("[Ok]");
        return 0;
    }

    /**
     * Вывод очистки проекта
     */
    public int clearProject() {
        System.out.println("[Clear project]");
        projectService.clear();
        System.out.println("[Ok]");
        return 0;
    }

    /**
     * Просмотр проекта по индексу
     */
    public int viewProjectByIndex() {
        System.out.println("Enter, project index: ");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        return 0;
    }

    /**
     * Просмотр проекта по идентификатору
     */
    public int viewProjectById() {
        System.out.println("Enter, project id: ");
        final Long id = scanner.nextLong();
        final Project project = projectService.findById(id);
        viewProject(project);
        return 0;
    }

    /**
     * Просмотр списка проектов
     */
    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[View project]");
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("[Ok]");
    }

    /**
     * Вывод списка проектов
     */
    public int listProject() {
        System.out.println("[List project]");
        int index = 1;
        for (final Project project : projectService.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[Ok]");
        return 0;

    }
}
